﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EstrelaLivre.Web.Startup))]
namespace EstrelaLivre.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
