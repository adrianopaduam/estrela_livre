﻿using EstrelaLivre.Dados.Contexto;
using EstrelaLivre.Entidades.Modelos;
using EstrelaLivre.Entidades.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EstrelaLivre.Web.Controllers
{
    public class EditController : Controller
    {
        private ContextoDados db = new ContextoDados();

        // GET: Edit/Edit/ProdutoID
        public ActionResult Produto(int? id)
        {
            if (id != null)
            {
                Produto produto = db.Produto.Find(id);
                EditProdutoVM model = new EditProdutoVM(produto);

                ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", produto.TipoAlimentoID);
                return View(model);
            }

            return RedirectToAction("VisualizarProdutos", "Admin");
        }

        // POST: Edit/Edit/ProdutoID
        [HttpPost]
        public ActionResult Produto([Bind(Include="ProdutoID,Nome,TipoAlimentoID,Quantidade,UnidadeMedida,ImgFile")]EditProdutoVM produto_editado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Produto corrigido = db.Produto.Find(produto_editado.ProdutoID);
                            corrigido.Nome = produto_editado.Nome;
                            corrigido.TipoAlimentoID = produto_editado.TipoAlimentoID;
                            corrigido.Quantidade = produto_editado.Quantidade;
                            corrigido.UnidadeMedida = produto_editado.UnidadeMedida;

                    byte[] imagem_binaria;

                    if (produto_editado.ImgFile != null)
                    {
                        try
                        {
                            Stream arquivoStream = produto_editado.ImgFile.InputStream;
                            imagem_binaria = new byte[arquivoStream.Length];
                            arquivoStream.Read(imagem_binaria, 0, imagem_binaria.Length);

                            if (imagem_binaria != null)
                            {
                                corrigido.Imagem = imagem_binaria;
                            }
                            else
                            {
                                ViewBag.Erro = "Falha na leitura da imagem";
                                ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", produto_editado.TipoAlimentoID);
                                return View();
                            }

                        }
                        catch (Exception ex)
                        {
                            ViewBag.Erro = " Erro " + ex.Message;
                            ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", produto_editado.TipoAlimentoID);
                            return View();
                        }
                    }

                    db.SaveChanges();
                    return RedirectToAction("VisualizarProdutos", "Admin");
                }

                ViewBag.Erro = "Favor Preencher todos os Campos Corretamente.";
                ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", produto_editado.TipoAlimentoID);
                return View();
            }
            catch(Exception e)
            {
                ViewBag.Erro = "Erro de conexão com o banco de dados. Contate seu Administrador.";
                ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", produto_editado.TipoAlimentoID);
                return View();
            }
        }

        // GET: Edit/Edit/OpcionalID
        public ActionResult Opcional(int? id)
        {
            if (id != null)
            {
                Opcional opcional = db.Opcional.Find(id);
                var model = new EditOpcionalVM(opcional);

                ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", opcional.TipoAlimentoID);
                return View(model);
            }

            return RedirectToAction("VisualizarOpcionais", "Admin");
        }

        // POST: Edit/Edit/OpcionalID
        [HttpPost]
        public ActionResult Opcional([Bind(Include = "OpcionalID,Nome,TipoAlimentoID,Quantidade,UnidadeMedida,Valor,ImgFile")]EditOpcionalVM opcional_editado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Opcional corrigido = db.Opcional.Find(opcional_editado.OpcionalID);
                    corrigido.Nome = opcional_editado.Nome;
                    corrigido.TipoAlimentoID = opcional_editado.TipoAlimentoID;
                    corrigido.Quantidade = opcional_editado.Quantidade;
                    corrigido.UnidadeMedida = opcional_editado.UnidadeMedida;
                    corrigido.Valor = opcional_editado.Valor;

                    byte[] imagem_binaria;

                    if (opcional_editado.ImgFile != null)
                    {
                        try
                        {
                            Stream arquivoStream = opcional_editado.ImgFile.InputStream;
                            imagem_binaria = new byte[arquivoStream.Length];
                            arquivoStream.Read(imagem_binaria, 0, imagem_binaria.Length);

                            if (imagem_binaria != null)
                            {
                                corrigido.Imagem = imagem_binaria;
                            }
                            else
                            {
                                ViewBag.Erro = "Falha na leitura da imagem";
                                ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", opcional_editado.TipoAlimentoID);
                                return View();
                            }

                        }
                        catch (Exception ex)
                        {
                            ViewBag.Erro = " Erro " + ex.Message;
                            ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", opcional_editado.TipoAlimentoID);
                            return View();
                        }
                    }

                    db.SaveChanges();
                    return RedirectToAction("VisualizarOpcionais", "Admin");
                }

                ViewBag.Erro = "Favor Preencher todos os Campos Corretamente.";
                ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", opcional_editado.TipoAlimentoID);
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Erro = "Erro de conexão com o banco de dados. Contate seu Administrador.";
                ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoID", "Descricao", opcional_editado.TipoAlimentoID);
                return View();
            }
        }

        // GET: Edit/Edit/EmpresaID
        public ActionResult Empresa(int? id)
        {
            if (id != null)
            {
                Empresa empresa = db.Empresa.Find(id);
                return View(empresa);
            }

            return RedirectToAction("VisualizarEmpresas", "Admin");
        }

        // POST: Edit/Edit/ProdutoID
        [HttpPost]
        public ActionResult Empresa(Empresa modificada)
        {
            if (ModelState.IsValid)
            {
                db.Entry(modificada).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("VisualizarEmpresas", "Admin");
            }
            ViewBag.Erro = "Favor Preencher os Campos Corretamente.";
            return View();
        }

    }
}
