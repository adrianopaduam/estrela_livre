﻿using EstrelaLivre.Dados.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using EstrelaLivre.Entidades.ViewModels;
using System.Net;
using System.Threading.Tasks;
using EstrelaLivre.Entidades.Modelos;
using System.IO;
using System.Drawing;
using System.Globalization;

namespace EstrelaLivre.Web.Controllers
{
    public class AdminController : Controller
    {
        private ContextoDados db = new ContextoDados();
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult getProdImageById(int id)
        {
            var imagem = db.Produto.Find(id).Imagem;

            return File(imagem, "image/png");
        }

        public ActionResult getOpicImageById(int id)
        {
            var imagem = db.Opcional.Find(id).Imagem;

            return File(imagem, "image/png");
        }

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        //GET: Gerenciar Administradores
        public ActionResult GerenciarAdministradores()
        {
            var userRoles = new List<RolesViewModel>();

            foreach (var user in UserManager.Users)
            {
                var userRole = new RolesViewModel()
                {
                    UserName = user.UserName,
                    UserID = user.Id,
                    RoleName = string.Join("", UserManager.GetRoles(user.Id))
                };
                
                userRoles.Add(userRole);
            }
            return View(userRoles);
        }

        //POST: Gerenciar Administradores
        [HttpPost]
        public ActionResult GerenciarAdministradores(string UserId, string UserRole)
        {
            if (UserId == null)
            {
                return View("Error");
            }

            UserManager.RemoveFromRole(UserId, UserRole);

            if (UserRole == "Administrador")
            {
                UserManager.AddToRole(UserId, "Usuario");
            }
            else
            {
                UserManager.AddToRole(UserId, "Administrador");
            }

            var userRoles = new List<RolesViewModel>();

            foreach (var user in UserManager.Users)
            {
                var userRole = new RolesViewModel()
                {
                    UserName = user.UserName,
                    UserID = user.Id,
                    RoleName = string.Join("", UserManager.GetRoles(user.Id))
                };

                userRoles.Add(userRole);
            }

            return View(userRoles);
        }

        //GET: Cadastrar Produto
        public ActionResult CadastrarProduto()
        {
            ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoId", "Descricao");
            return View();
        }

        //POST: Cadastrar Produto
        [HttpPost]
        public ActionResult CadastrarProduto([Bind(Include = "Nome,TipoAlimentoID,Quantidade,UnidadeMedida,ImgFile")] ProdutoViewModel model)
        {
            if (ModelState.IsValid)
            {
                Produto novo_produto = new Produto(model);
                byte[] imagem_binaria;

                if (model.ImgFile != null)
                {
                    try
                    {
                        Stream arquivoStream = model.ImgFile.InputStream;
                        imagem_binaria = new byte[arquivoStream.Length];
                        arquivoStream.Read(imagem_binaria, 0, imagem_binaria.Length);
                        
                        if (imagem_binaria != null)
                        {
                            novo_produto.Imagem = imagem_binaria;
                        }
                        else
                        {
                            ViewBag.Message = "Falha na leitura da imagem";
                            ViewBag.Status = "FAIL";
                            ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoId", "Descricao");
                            return View();
                        }

                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = " Erro " + ex.Message;
                        ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoId", "Descricao");
                        return View();
                    }
                }

                else
                {
                    novo_produto.Imagem = null;
                }

                db.Produto.Add(novo_produto);
                db.SaveChanges();
                
            }
            else
            {
                ViewBag.Error = true;
            }

            if (ViewBag.Error == null)
            {
                ViewBag.Message = "Produto salvo com sucesso!";
                ViewBag.Status = "OK";
            }
            else
            {
                ViewBag.Message = "Dados Inválidos. Tente novamente";
                ViewBag.Status = "FAIL";
            }

            ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoId", "Descricao");
            return View();
        }

        //GET: Vizualizar Produtos
        public ActionResult VisualizarProdutos()
        {
            var lista_produtos = db.Produto.ToList();
            lista_produtos.OrderBy(x => x.Nome);

            var model = new List<VisualizarProdutoVM>();
            foreach (var prod in lista_produtos)
            {
                var visualizador = new VisualizarProdutoVM()
                {
                    ProdutoID = prod.ProdutoID,
                    Nome = prod.Nome,
                    TipoAlimento = prod.TipoAlimento.Descricao,
                    UnidadeVenda = prod.Quantidade + " " + prod.UnidadeMedida,
                };

                model.Add(visualizador);
            }

            //Verificação se a Ação de deleção foi realizada para exibição de texto
            bool confirma_deleção = false;
            if (Session["Deleted"] != null)
                confirma_deleção = (bool)Session["Deleted"];

            if (confirma_deleção)
            {
                ViewBag.Deleted = true;
                Session["Deleted"] = false;
            }
            else
                ViewBag.Deleted = null;

            return View(model);
        }

        //GET: Cadastrar Opcional
        public ActionResult CadastrarOpcional()
        {
            ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoId", "Descricao");
            return View();
        }

        //POST: Cadastrar Opcional
        [HttpPost]
        public ActionResult CadastrarOpcional([Bind(Include = "Nome,TipoAlimentoID,Quantidade,UnidadeMedida,Valor,ImgFile")] OpcionalViewModel model)
        {
            if (ModelState.IsValid)
            {
                Opcional novo_opcional = new Opcional(model);
                byte[] imagem_binaria;

                if (model.ImgFile != null)
                {
                    try
                    {
                        Stream arquivoStream = model.ImgFile.InputStream;
                        imagem_binaria = new byte[arquivoStream.Length];
                        arquivoStream.Read(imagem_binaria, 0, imagem_binaria.Length);

                        if (imagem_binaria != null)
                        {
                            novo_opcional.Imagem = imagem_binaria;
                        }
                        else
                        {
                            ViewBag.Message = "Falha na leitura da imagem";
                            ViewBag.Status = "FAIL";
                            ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoId", "Descricao");
                            return View();
                        }

                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = " Erro " + ex.Message;
                        ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoId", "Descricao");
                        return View();
                    }
                }

                else
                {
                    novo_opcional.Imagem = null;
                }

                db.Opcional.Add(novo_opcional);
                db.SaveChanges();

            }
            else
            {
                ViewBag.Error = true;
            }

            if (ViewBag.Error == null)
            {
                ViewBag.Message = "Produto salvo com sucesso!";
                ViewBag.Status = "OK";
            }
            else
            {
                ViewBag.Message = "Dados Inválidos. Tente novamente";
                ViewBag.Status = "FAIL";
            }

            ViewBag.TipoAlimentoID = new SelectList(db.TipoAlimento, "TipoAlimentoId", "Descricao");
            return View();
        }

        //GET: Vizualizar Opcionais
        public ActionResult VisualizarOpcionais()
        {
            var lista_opcionais = db.Opcional.OrderBy(x => x.Nome).ToList();

            var model = new List<VisualizarOpcionalVM>();
            foreach (var prod in lista_opcionais)
            {
                VisualizarOpcionalVM visualizador = new VisualizarOpcionalVM()
                {
                    OpcionalID = prod.OpcionalID,
                    Nome = prod.Nome,
                    TipoAlimento = prod.TipoAlimento.Descricao,
                    UnidadeVenda = prod.Quantidade + " " + prod.UnidadeMedida,
                    PrecoVenda = String.Format(new CultureInfo("pt-BR"), "{0:C}", prod.Valor)
                };

                model.Add(visualizador);
            }

            //Verificação se a Ação de deleção foi realizada para exibição de texto
            bool confirma_deleção = false;
            if (Session["Deleted"] != null)
                confirma_deleção = (bool)Session["Deleted"];

            if (confirma_deleção)
            {
                ViewBag.Deleted = true;
                Session["Deleted"] = false;
            }
            else
                ViewBag.Deleted = null;

            return View(model);
        }

        

        //GET: Cadastrar Empresa
        public ActionResult CadastrarEmpresa()
        {
            return View();
        }

        //POST: Cadastrar Empresa
        [HttpPost]
        public ActionResult CadastrarEmpresa(Empresa nova_empresa)
        {
            try
            {
                db.Empresa.Add(nova_empresa);
                db.SaveChanges();
                ViewBag.Status = "OK";
                ViewBag.Message = "Empresa Cadastrada com Sucesso!";
            }
            catch (Exception unique_key_exception)
            {
                ViewBag.Status = "FAIL";
                ViewBag.Message = "CNPJ já utilizado. Favor Verificar os Dados Inseridos";
            }

            return View();
        }

        //GET: Visualizar Empresas
        public ActionResult VisualizarEmpresas()
        {
            var lista_empresas = new List<EmpresaViewModel>();

            foreach (var empresa in db.Empresa.ToList())
            {
                var empresaVM = new EmpresaViewModel()
                {
                    EmpresaID = empresa.EmpresaID,
                    Nome = empresa.Nome,
                    CNPJ = empresa.CNPJ,
                    Endereco = empresa.Logradouro + ", " + empresa.Numero + ", " + empresa.Bairro + ".",
                    Cidade = empresa.Cidade
                };

                lista_empresas.Add(empresaVM);
            }

            //Verificação se a Ação de deleção foi realizada para exibição de texto
            bool confirma_deleção = false;
            if (Session["Deleted"] != null)
                confirma_deleção = (bool)Session["Deleted"];

            if (confirma_deleção)
            {
                ViewBag.Deleted = true;
                Session["Deleted"] = false;
            }
            else
                ViewBag.Deleted = null;
 
            return View(lista_empresas);
        }
    }
}