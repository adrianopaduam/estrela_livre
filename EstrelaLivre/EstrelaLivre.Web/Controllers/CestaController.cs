﻿using EstrelaLivre.Dados.Contexto;
using EstrelaLivre.Entidades.Modelos;
using EstrelaLivre.Entidades.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace EstrelaLivre.Web.Controllers
{
    public class CestaController : Controller
    {
        private ContextoDados db = new ContextoDados();

        //GET: Configurar Cestas
        public ActionResult ConfigurarCesta()
        {
            return View();
        }

        // GET: Disponibilizar Produtos
        public ActionResult DisponibilizarProdutos()
        {
            List<Produto> lista_produtos = db.Produto.OrderBy(p => p.Nome).ToList();

            ViewBag.Contador = (int)lista_produtos.Count();

            return View(lista_produtos);
        }

        // GET: Disponibilizar Opcionais
        public ActionResult DisponibilizarOpcionais()
        {
            List<Opcional> lista_opcionais = db.Opcional.OrderBy(p => p.Nome).ToList();

            ViewBag.Contador = (int)lista_opcionais.Count();

            return View(lista_opcionais);
        }

        //POST: Alterar Disponibilidade
        [HttpPost]
        public ActionResult AlterarDisponibilidade(int? id, string tipo)
        {
            if (tipo == "Produto")
            {
                var selecionado = db.Produto.Find(id);
                if (selecionado.Disponivel)
                {
                    selecionado.Disponivel = false;
                }
                else
                {
                    selecionado.Disponivel = true;
                }
                db.SaveChanges();

                return RedirectToAction("DisponibilizarProdutos");
            }
            else if (tipo == "Opcional")
            {
                var selecionado = db.Opcional.Find(id);
                if (selecionado.Disponivel)
                {
                    selecionado.Disponivel = false;
                }
                else
                {
                    selecionado.Disponivel = true;
                }
                db.SaveChanges();

                return RedirectToAction("DisponibilizarOpcionais");
            }

            return View();
        }

        //GET: Definir Quantidades
        public ActionResult DefinirQuantidades()
        {
            var definirVM = new DefinirViewModel();
            definirVM.lista_alimentos = db.TipoAlimento.Where(tpa => tpa.TipoAlimentoID <= 5).ToList();
            definirVM.quantidades = db.QuantidadesCB.Select(qtd => qtd.Quantidade).ToList();
            definirVM.ValorAtual = db.ValorCB.ToList().Last().ValorAtual;
            
            return View(definirVM);
        }

        //POST: Definir Quantidades e Valor Básico
        [HttpPost]
        public ActionResult DefinirQuantidades([Bind(Include = "lista_alimentos,quantidades,ValorAtual")] DefinirViewModel model)
        {
            model.lista_alimentos = db.TipoAlimento.Where(tpa => tpa.TipoAlimentoID <= 5).ToList();

            List<QuantidadesCB> lista_atualizada = new List<QuantidadesCB>();
            foreach (var alimento in model.lista_alimentos)
            {
                var linha_tabela = db.QuantidadesCB.Find(alimento.TipoAlimentoID);
                linha_tabela.Quantidade = model.quantidades[alimento.TipoAlimentoID - 1];
                lista_atualizada.Add(linha_tabela);
            }

            var valor_antigo = db.ValorCB.Where(x => x.DataTermino == null).First();
            valor_antigo.DataTermino = DateTime.Now;


            var valor_atualizado = new ValorCB();
            valor_atualizado.ValorAtual = model.ValorAtual;
            valor_atualizado.DataInicio = DateTime.Now;
            valor_atualizado.DataTermino = null;
            db.ValorCB.Add(valor_atualizado);

            db.SaveChanges();

            return RedirectToAction("ConfigurarCesta");
        }

        //Get: Nova Cesta
        public ActionResult NovaCesta()
        {
            int ultimo_id;
            CestaBasica ultima_cesta;
            /*
            try
            {
                ultima_cesta = db.CestaBasica.OrderByDescending(cb => cb.CestaBasicaID).First();
                ultimo_id = ultima_cesta.CestaBasicaID;
                Session["CestaBasicaID"] = ultima_cesta.CestaBasicaID;
            }
            catch (Exception e)
            {
                ultima_cesta = new CestaBasica()
                {
                    CestaBasicaID = 1,
                    UsuarioID = "1234",
                    ValorTotal = db.ValorCB.OrderByDescending(vcb => vcb.DataTermino).First().ValorAtual,
                    FormaPagamento = null,
                    Finalizada = false
                };
                db.CestaBasica.Add(ultima_cesta);
                db.SaveChanges();
                Session["CestaBasicaID"] = ultima_cesta.CestaBasicaID;
            }
            */
            var model = new NovaCestaViewModel();
            model.produtos = db.Produto.Where(p => p.Disponivel == true).ToList();
            model.opcionais = db.Opcional.Where(op => op.Disponivel == true).ToList();

            return View(model);
        }

        //POST: Adicionar Prodtuos ao Carrinho
        [HttpPost]
        public ActionResult AdicionarAoCarrinho(int id, string tipo)
        {
            int cestabasicaID = 1;
            bool relacaoexistente = false;

            if (tipo == "Produto")
            {
                RelacaoCestaProduto cestaproduto = db.CestaProduto.Find(cestabasicaID, id);

                if (cestaproduto == null)
                {
                    cestaproduto = new RelacaoCestaProduto()
                    {
                        CestabasicaID = cestabasicaID,
                        ProdutoID = id,
                        Quantidade = 1
                    };
                }
                else
                {
                    relacaoexistente = true;
                    cestaproduto.Quantidade += 1;
                }


                if (!relacaoexistente)
                    db.CestaProduto.Add(cestaproduto);
                db.SaveChanges();
            }

            if (tipo == "Opcional")
            {
                RelacaoCestaOpcional cestaopcional = db.CestaOpcional.Find(cestabasicaID, id);
                    
                if (cestaopcional == null)
                {
                    cestaopcional = new RelacaoCestaOpcional()
                    {
                        CestabasicaID = cestabasicaID,
                        OpcionalID = id,
                        Quantidade = 1,
                        ValorTotal = db.Opcional.Find(id).Valor
                    };
                }
                else
                {
                    relacaoexistente = true;
                    cestaopcional.Quantidade += 1;
                    cestaopcional.ValorTotal += db.Opcional.Find(id).Valor;
                }


                if (!relacaoexistente)
                    db.CestaOpcional.Add(cestaopcional);
                db.SaveChanges();
            }

            return RedirectToAction("NovaCesta");
        }
    }
}