﻿using EstrelaLivre.Dados.Contexto;
using EstrelaLivre.Entidades.Modelos;
using EstrelaLivre.Entidades.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EstrelaLivre.Web.Controllers
{
    public class DeleteController : Controller
    {
        private ContextoDados db = new ContextoDados();

        // GET: Delete/Produto
        public ActionResult Produto(int id)
        {
            Produto deleteted_confirmed = db.Produto.Find(id);
            db.Produto.Remove(deleteted_confirmed);
            db.SaveChanges();

            Session["Deleted"] = true;
            return RedirectToAction("VisualizarProdutos", "Admin");
        }

        // GET: Delete/Opcional
        public ActionResult Opcional(int id)
        {
            Opcional deleteted_confirmed = db.Opcional.Find(id);
            db.Opcional.Remove(deleteted_confirmed);
            db.SaveChanges();

            Session["Deleted"] = true;
            return RedirectToAction("VisualizarOpcionais", "Admin");
        }

        // GET: Delete/Empresa
        public ActionResult Empresa(int id)
        {
            Empresa deleteted_confirmed = db.Empresa.Find(id);
            db.Empresa.Remove(deleteted_confirmed);
            db.SaveChanges();

            Session["Deleted"] = true;
            return RedirectToAction("VisualizarEmpresas", "Admin");
        }
    }
}