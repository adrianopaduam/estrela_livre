﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace EstrelaLivre.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Indicator = "Home";

            return View();
        }

        public ActionResult Admin()
        {
            return View();
        }
    }
}