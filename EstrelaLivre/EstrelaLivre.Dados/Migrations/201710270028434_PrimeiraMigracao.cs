namespace EstrelaLivre.Dados.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrimeiraMigracao : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TipoAlimentoes", "Descricao", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TipoAlimentoes", "Descricao", c => c.String());
        }
    }
}
