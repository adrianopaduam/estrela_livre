namespace EstrelaLivre.Dados.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TipoAlimentoes",
                c => new
                    {
                        TipoAlimentoId = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                    })
                .PrimaryKey(t => t.TipoAlimentoId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TipoAlimentoes");
        }
    }
}
