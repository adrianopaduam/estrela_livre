namespace EstrelaLivre.Dados.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EstrelaLivre.Dados.Contexto.ContextoDados>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "EstrelaLivre.Dados.Contexto.ContextoDados";
        }

        protected override void Seed(EstrelaLivre.Dados.Contexto.ContextoDados context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
