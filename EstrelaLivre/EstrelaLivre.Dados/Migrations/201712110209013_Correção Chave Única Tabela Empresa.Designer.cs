// <auto-generated />
namespace EstrelaLivre.Dados.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class CorreçãoChaveÚnicaTabelaEmpresa : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CorreçãoChaveÚnicaTabelaEmpresa));
        
        string IMigrationMetadata.Id
        {
            get { return "201712110209013_Correção Chave Única Tabela Empresa"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
