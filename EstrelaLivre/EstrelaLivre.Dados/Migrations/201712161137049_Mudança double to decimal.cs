namespace EstrelaLivre.Dados.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mudançadoubletodecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Opcional", "Valor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Opcional", "Valor", c => c.Double(nullable: false));
        }
    }
}
