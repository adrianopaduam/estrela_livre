﻿
using EstrelaLivre.Entidades.Modelos;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace EstrelaLivre.Dados.Contexto
{
    public class ContextoDados : DbContext
    {
        public ContextoDados()
            : base("DadosConnection")
        {
            //this.Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<TipoAlimento> TipoAlimento { get; set; }

        public DbSet<FormaPagamento> FormaPagamento { get; set; }

        public DbSet<CestaBasica> CestaBasica { get; set; }

        public DbSet<Empresa> Empresa { get; set; }

        public DbSet<Produto> Produto { get; set; }

        public DbSet<Opcional> Opcional { get; set; }

        public DbSet<Login> Login { get; set; }

        public DbSet<RelacaoCestaProduto> CestaProduto { get; set; }

        public DbSet<RelacaoCestaOpcional> CestaOpcional { get; set; }

        public DbSet<QuantidadesCB> QuantidadesCB { get; set; }

        public DbSet<ValorCB> ValorCB { get; set; }
    }
}
