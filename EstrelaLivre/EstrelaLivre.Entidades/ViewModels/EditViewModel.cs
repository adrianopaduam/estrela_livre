﻿using EstrelaLivre.Entidades.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EstrelaLivre.Entidades.ViewModels
{
    public class EditProdutoVM
    {
        [Required]
        public int ProdutoID { get; set; }

        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Tipo de Alimento")]
        public int TipoAlimentoID { get; set; }

        [Required]
        [Display(Name = "Quantidade")]
        public int Quantidade { get; set; }

        [Required]
        [Display(Name = "Unidade de Medida")]
        public string UnidadeMedida { get; set; }

        [Display(Name = "Imagem")]
        public HttpPostedFileBase ImgFile { get; set; }

        public EditProdutoVM(Produto produto)
        {
            ProdutoID = produto.ProdutoID;
            Nome = produto.Nome;
            Quantidade = produto.Quantidade;
            UnidadeMedida = produto.UnidadeMedida;
        }

        public EditProdutoVM() { }
    }

    public class EditOpcionalVM
    {
        [Required]
        public int OpcionalID { get; set; }

        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Tipo de Alimento")]
        public int TipoAlimentoID { get; set; }

        [Required]
        [Display(Name = "Quantidade")]
        public int Quantidade { get; set; }

        [Required]
        [Display(Name = "Unidade de Medida")]
        public string UnidadeMedida { get; set; }

        [Required]
        [Display(Name = "Valor de Venda")]
        public decimal Valor { get; set; }

        [Display(Name = "Imagem")]
        public HttpPostedFileBase ImgFile { get; set; }

        public EditOpcionalVM(Opcional opcional)
        {
            OpcionalID = opcional.OpcionalID;
            Nome = opcional.Nome;
            Quantidade = opcional.Quantidade;
            UnidadeMedida = opcional.UnidadeMedida;
            Valor = opcional.Valor;
        }

        public EditOpcionalVM() { }
    }
}
