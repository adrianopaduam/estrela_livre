﻿using EstrelaLivre.Entidades.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EstrelaLivre.Entidades.ViewModels
{
    public class NovaCestaViewModel
    {
        public List<Produto> produtos { get; set; }

        public List<Opcional> opcionais { get; set; }
    }

    public class DefinirViewModel
    {
        [Display(Name = "Lista Alimentos")]
        public List<TipoAlimento> lista_alimentos { get; set; }

        [Range(1,5)]
        public List<int> quantidades { get; set; }

        [Display(Name = "Valor Base")]
        [Range(0.01, 99.99)]
        public decimal ValorAtual { get; set; }
    }
}
