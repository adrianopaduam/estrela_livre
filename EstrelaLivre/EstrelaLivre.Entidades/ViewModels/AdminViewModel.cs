﻿using EstrelaLivre.Entidades.Modelos;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;


namespace EstrelaLivre.Entidades.ViewModels
{
    public class RolesViewModel
    {
        public string UserName { get; set; }

        public string UserID { get; set; }

        public string RoleName { get; set; }
    }

    public class ProdutoViewModel
    {
        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Tipo de Alimento")]
        public int TipoAlimentoID { get; set; }

        [Required]
        [Display(Name = "Quantidade")]
        public int Quantidade { get; set; }

        [Required]
        [Display(Name = "Unidade de Medida")]
        public string UnidadeMedida { get; set; }

        [Display(Name = "Imagem")]
        public HttpPostedFileBase ImgFile { get; set; }

        public ProdutoViewModel() { }

        public ProdutoViewModel(Produto produto)
        {
            Nome = produto.Nome;
            TipoAlimentoID = produto.TipoAlimentoID;
            Quantidade = produto.Quantidade;
            UnidadeMedida = produto.UnidadeMedida;
        }

    }

    public class VisualizarProdutoVM
    {
        public int ProdutoID { get; set; }

        [StringLength(45)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Display(Name = "Tipo de Alimento")]
        public string TipoAlimento { get; set; }

        [Display(Name = "Unidade de Venda")]
        public string UnidadeVenda { get; set; }

    }

    public class OpcionalViewModel
    {
        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Tipo de Alimento")]
        public int TipoAlimentoID { get; set; }

        [Required]
        [Display(Name = "Quantidade")]
        public int Quantidade { get; set; }

        [Required]
        [Display(Name = "Unidade de Medida")]
        public string UnidadeMedida { get; set; }

        [Required]
        [Display(Name = "Valor")]
        [DataType(DataType.Currency)]
        [Range(0.01, 999.99)]
        public decimal Valor { get; set; }

        [Display(Name = "Imagem")]
        public HttpPostedFileBase ImgFile { get; set; }

        public OpcionalViewModel() { }

        public OpcionalViewModel(Opcional opcional)
        {
            Nome = opcional.Nome;
            TipoAlimentoID = opcional.TipoAlimentoID;
            Quantidade = opcional.Quantidade;
            UnidadeMedida = opcional.UnidadeMedida;
            Valor = opcional.Valor;
        }

    }

    public class VisualizarOpcionalVM
    {
        public int OpcionalID { get; set; }

        [StringLength(45)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Display(Name = "Tipo de Alimento")]
        public string TipoAlimento { get; set; }

        [Display(Name = "Unidade de Venda")]
        public string UnidadeVenda { get; set; }

        [Display(Name = "Valor de Venda")]
        public string PrecoVenda { get; set; }

    }

    

    public class EmpresaViewModel
    {
        public int EmpresaID { get; set; }

        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "CNPJ")]
        public string CNPJ { get; set; }

        [Required]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        [Required]
        [Display(Name = "Cidade")]
        public string Cidade { get; set; }
    }
}