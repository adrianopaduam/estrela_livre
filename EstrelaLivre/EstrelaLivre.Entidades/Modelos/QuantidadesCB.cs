﻿namespace EstrelaLivre.Entidades.Modelos
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("QuantidadesCB")]
    public partial class QuantidadesCB
    {
        [Key]
        [Required]
        [Display(Name = "Tipo de Alimento")]
        public int TipoAlimentoID { get; set; }

        [ForeignKey("TipoAlimentoID")]
        public virtual TipoAlimento TipoAlimento { get; set; }

        [Required]
        public int Quantidade { get; set; }
    }
}