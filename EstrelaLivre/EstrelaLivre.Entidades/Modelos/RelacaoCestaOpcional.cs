﻿namespace EstrelaLivre.Entidades.Modelos
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("RelacaoCestaOpcional")]
    public partial class RelacaoCestaOpcional
    {
        [Key, Column(Order = 0)]
        public int CestabasicaID { get; set; }

        [Key, Column(Order = 1)]
        public int OpcionalID { get; set; }

        [Required]
        public int Quantidade { get; set; }

        [Required]
        public decimal ValorTotal { get; set; }

    }
}
