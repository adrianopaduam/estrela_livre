﻿
namespace EstrelaLivre.Entidades.Modelos
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Login")]
    public partial class Login
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int LoginID { get; set; }

        public string Apelido { get; set; }

        public string Email { get; set; }

        public int EmpresaID { get; set; }

        public Login()
        {

        }

        public Login(string Apelido, string Email, int EmpresaID)
        {
            this.Apelido = Apelido;
            this.Email = Email;
            this.EmpresaID = EmpresaID;
        }
    }
}
