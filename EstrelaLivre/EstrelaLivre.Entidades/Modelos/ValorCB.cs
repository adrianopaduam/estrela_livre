﻿namespace EstrelaLivre.Entidades.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ValorCB")]
    public partial class ValorCB
    {
        [Key]
        [Column(Order = 0)]
        [Required]
        [Display(Name = "Valor Atual")]
        public decimal ValorAtual { get; set; }

        [Key]
        [Column(Order = 1)]
        [Required]
        public DateTime DataInicio { get; set; }

        
        public DateTime? DataTermino { get; set; }
    }
}
