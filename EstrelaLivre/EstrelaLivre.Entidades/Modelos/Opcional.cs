﻿namespace EstrelaLivre.Entidades.Modelos
{
    using EstrelaLivre.Entidades.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Opcional")]
    public partial class Opcional
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OpcionalID { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Tipo de Alimento")]
        public int TipoAlimentoID { get; set; }

        [ForeignKey("TipoAlimentoID")]
        public virtual TipoAlimento TipoAlimento { get; set; }

        [Required]
        [Display(Name = "Quantidade")]
        [Range(1, 999)]
        public int Quantidade { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Unidade de Medida")]
        public string UnidadeMedida { get; set; }

        [Required]
        [Display(Name = "Valor")]
        public decimal Valor { get; set; }

        [Display(Name = "Imagem")]
        public byte[] Imagem { get; set; }

        public bool Disponivel { get; set; }

        public Opcional(){ }

        public Opcional(OpcionalViewModel model)
        {
            Nome = model.Nome;
            TipoAlimentoID = model.TipoAlimentoID;
            Quantidade = model.Quantidade;
            UnidadeMedida = model.UnidadeMedida.ToUpper();
            Valor = model.Valor;
        }
    }
}
