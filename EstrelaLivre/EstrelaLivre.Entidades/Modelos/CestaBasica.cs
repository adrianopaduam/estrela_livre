namespace EstrelaLivre.Entidades.Modelos
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("CestaBasica")]
    public partial class CestaBasica
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int CestaBasicaID { get; set; }

        [Key]
        [Column(Order = 1)]
        public string UsuarioID { get; set; }

        public decimal ValorTotal { get; set; }

        [ForeignKey("FormaPagamento")]
        public int FormaPagamentoID { get; set; }

        public virtual FormaPagamento FormaPagamento { get; set; }

        [Required]
        public bool Finalizada { get; set; }
    }
}
