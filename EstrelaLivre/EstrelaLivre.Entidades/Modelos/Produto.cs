namespace EstrelaLivre.Entidades.Modelos
{
    using EstrelaLivre.Entidades.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Produto")]
    public partial class Produto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProdutoID { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Tipo de Alimento")]
        public int TipoAlimentoID { get; set; }
        
        [ForeignKey("TipoAlimentoID")]
        public virtual TipoAlimento TipoAlimento { get; set; }

        [Required]
        [Display(Name = "Quantidade")]
        [Range(1, 999)]
        public int Quantidade { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Unidade de Medida")]
        public string UnidadeMedida { get; set; }

        [Display(Name = "Imagem")]
        public byte[] Imagem { get; set; }

        public bool Disponivel { get; set; }

        public Produto(){ }


        public Produto(ProdutoViewModel model)
        {
            Nome = model.Nome;
            TipoAlimentoID = model.TipoAlimentoID;
            Quantidade = model.Quantidade;
            UnidadeMedida = model.UnidadeMedida.ToUpper();
        }
    }
}
