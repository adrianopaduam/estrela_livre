﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstrelaLivre.Entidades.Modelos
{
    //[Table("TipoAlimento")]
    public class TipoAlimento
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TipoAlimentoID { get; set; }

        [Required]
        public string Descricao { get; set; }
    }
}
