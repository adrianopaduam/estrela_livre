﻿namespace EstrelaLivre.Entidades.Modelos
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("RelacaoCestaProduto")]
    public partial class RelacaoCestaProduto
    {
        [Key, Column(Order=0)]
        public int CestabasicaID { get; set; }

        [Key, Column(Order = 1)]
        public int ProdutoID { get; set; }

        [Required]
        public int Quantidade { get; set; }

    }
}
