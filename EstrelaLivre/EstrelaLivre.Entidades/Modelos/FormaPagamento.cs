namespace EstrelaLivre.Entidades.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormaPagamento")]
    public partial class FormaPagamento
    {
        [Key]
        public int FormaPagamentoID { get; set; }

        [Required]
        [StringLength(50)]
        public string Descricao { get; set; }
    }
}
